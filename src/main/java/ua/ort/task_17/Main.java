package ua.ort.task_17;

import ua.ort.task_17.subjects.Point;

public class Main {

	static int calculatePoints(int x, int y) {
		return ((int) Math.pow(x, 2) + (int) Math.pow(y, 2));
	}

	static Point[] sortPoints(Point[] coordinats) {
		for (int i = coordinats.length; i > 1; i--) {
			for (int j = 1; j < i; j++) {
				int z = calculatePoints(coordinats[j - 1].getX(), coordinats[j - 1].getY());
				int z1 = calculatePoints(coordinats[j].getX(), coordinats[j].getY());
				if (z > z1) {
					Point temp = coordinats[j - 1];
					coordinats[j - 1] = coordinats[j];
					coordinats[j] = temp;
				}
			}
		}
		return coordinats;
	}

	static void printArrayPoints(Point[] points) {
		for (Point point : points) {
			System.out.println("Name: " + point.getName() + "\nX: " + point.getX() + "\nY: " + point.getY()
					+ "\nPoint: " + ((point.getX() * point.getX()) + (point.getY() * point.getY())));
			System.out.println();
		}
	}

	public static void main(String[] args) {
		Point point1 = new Point();
		Point point2 = new Point();
		Point point3 = new Point();

		point1.setName("Point1");
		point1.setX(10);
		point1.setY(11);

		point2.setName("Point2");
		point2.setX(9);
		point2.setY(12);

		point3.setName("Point3");
		point3.setX(8);
		point3.setY(3);

		Point[] points = { point1, point2, point3 };
		System.out.println("--------------");
		System.out.println("Current data");
		System.out.println("--------------");
		printArrayPoints(points);

		Point[] sortPoints = sortPoints(points);
		System.out.println("----------");
		System.out.println("Sort data");
		System.out.println("----------");
		printArrayPoints(sortPoints);

	}
}
