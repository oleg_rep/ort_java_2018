package ua.ort.task25.services;

public class PointParser {

	private String stringArea(String classParser, String fromChar, String toChar) {
		int startPosition = classParser.indexOf(fromChar);
		int endPosition = classParser.indexOf(toChar);
		return classParser.substring(startPosition, endPosition);
	}

	public String parseString(String json) {
		StringBuilder result = new StringBuilder();

		String className = stringArea(json, "Point", "{");
		String stringX = stringArea(json, "x", "y");
		String stringY = stringArea(json, "y", "name");
		String name = stringArea(json, "name", "}");

		int x = Integer.parseInt(stringX.substring(2, stringX.indexOf(",")));
		int y = Integer.parseInt(stringY.substring(2, stringY.indexOf(",")));

		result.append("class ").append(className).append("{\n\tprivate int x = ").append(x)
				.append(";\n\tprivate int y = ").append(y).append(";\n\tprivate String name = \"")
				.append(name.substring(6, name.lastIndexOf(" "))).append(name.substring(name.lastIndexOf(" ") + 1))
				.append("\";\n}");

		return result.toString();
	}
}
