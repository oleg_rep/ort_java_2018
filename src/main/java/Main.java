import ua.ort.task25.services.PointParser;

public class Main {
	public static void main(String[] args) {
		PointParser parser = new PointParser();
		String json = "Point {x:15, y:40, name: Upper Left}";
		System.out.println(parser.parseString(json));
	}
}
