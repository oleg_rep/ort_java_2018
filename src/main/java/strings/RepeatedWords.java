package strings;

import java.util.ArrayList;
import java.util.List;

public class RepeatedWords {

	private String firstSentance;
	private String secondSentance;

	public RepeatedWords(String firstSentance, String secondSentance) {

		this.firstSentance = firstSentance;
		this.secondSentance = secondSentance;
	}

	private List<String> checkWords() {

		String[] strArr1 = firstSentance.split(" ");
		String[] strArr2 = secondSentance.split(" ");

		List<String> repeatedWords = new ArrayList<String>();

		for (String s1 : strArr1) {
			for (String s2 : strArr2) {
				if (s1.equals(s2)) {
					repeatedWords.add(s1);
				}
			}
		}
		return repeatedWords;
	}

	public String buildResult() {
		List<String> print = checkWords();
		StringBuilder buildString = new StringBuilder();

		for (String string : print) {
			buildString.append(string).append(",");
		}

		int lastComa = buildString.lastIndexOf(",");
		String result = buildString.substring(0, lastComa).toString();

		return result.trim();
	}

	public void printResult() {
		System.out.println("Result: " + buildResult());
	}

	public static void main(String[] args) {

		String firstSentance = "hello world vasya";
		String secondSentance = "hello vasya hello";
		RepeatedWords str = new RepeatedWords(firstSentance, secondSentance);
		System.out.println("First sentance: " + firstSentance);
		System.out.println("First sentance: " + secondSentance);
		str.printResult();
	}
}