package strings;

import static org.junit.Assert.*;

import org.junit.Test;

public class buildResultResult {

	@Test
	public void buildResultTest() {

		String firstSentance = "hello world vasya";
		String secondSentance = "hello vasya hello";
		RepeatedWords str = new RepeatedWords(firstSentance, secondSentance);

		String expected = str.buildResult();
		String actual = "hello,hello,vasya";

		assertEquals(expected, actual);
	}
}
