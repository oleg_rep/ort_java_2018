package ua.ort.task25;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import ua.ort.task25.services.PointParser;

public class PointParserTest {

	@Test
	public void classParserTest() {
		PointParser task_25Test = new PointParser();
		String json = "Point {x:15, y:40, name: Upper Left}";
		String expected = task_25Test.parseString(json);
		String actual = "class Point {\n" + "\tprivate int x = 15;\n" + "\tprivate int y = 40;\n"
				+ "\tprivate String name = \"UpperLeft\";\n}";
		assertEquals(expected, actual);
	}
}
