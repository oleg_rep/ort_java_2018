package ua.ort.ort_homework.task_14;

import java.util.Scanner;

/**
 * 10.53. Написать рекурсивную процедуру для ввода с клавиатуры
 * последовательности чисел и вывода ее на экран в обратном порядке (окончание
 * последовательности — при вводе нуля).
 */
public class Recurcy {
	private static Scanner input;

	public static void main(String[] args) {
		System.out.println("Введите последовательность чисел");
		System.out.println("Результат: " + convertLineOfDdigits());
	}

	static String convertLineOfDdigits() {
		input = new Scanner(System.in);
		String n = input.nextLine();
		if (n.equals("0")) {
			return "";
		}
		return n = convertLineOfDdigits() + n + ",";
	}
}
