package ua.ort.ort_homework.task_14;

import java.util.Random;
import java.util.concurrent.CountDownLatch;

public class Sort {

	static int[][] fillTable(int sizeX, int sizeY) {
		Random rand = new Random();
		int[][] table = new int[sizeX][sizeY];
		for (int i = 0; i < sizeX; i++) {
			for (int j = 0; j < sizeY; j++) {
				table[i][j] = rand.nextInt(100);
			}
		}
		return table;
	}

	static int[][] sortArray(int[][] array, int columnNumber) {

		for (int i = array[0].length; i > 1; i--) {
			for (int j = 1; j < i; j++) {
				if (array[j - 1][columnNumber] > array[j][columnNumber]) {
					int temp = array[j - 1][columnNumber];
					array[j - 1][columnNumber] = array[j][columnNumber];
					array[j][columnNumber] = temp;
				}
			}
		}
		return array;
	}

	static void printSortedTable(int[][] sortedTable) {
		for (int i = 0; i < sortedTable.length; i++) {
			for (int j = 0; j < sortedTable[0].length; j++) {
				System.out.print(sortedTable[i][j] + " ");
			}
			System.out.println();
		}
	}

	public static void main(String[] args) {
		int sizeX = 4;
		int sizeY = 4;
		int sortedColumnNumber = 0;

		int[][] table = fillTable(sizeX, sizeY);
		System.out.println("Исходная таблица:");
		printSortedTable(table);
		System.out.println();
		int[][] sortedTable = sortArray(table, sortedColumnNumber);
		System.out.println("Таблица отсортированная по " + sortedColumnNumber + "-му столбцу");
		printSortedTable(sortedTable);
	}
}
