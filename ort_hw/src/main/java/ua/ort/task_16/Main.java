package ua.ort.task_16;

class Point {

	String name = null;
	int x = 0;
	int y = 0;

	public Point(String name, int x, int y) {
		this.name = name;
		this.x = x;
		this.y = y;
	}

}

public class Main {

	public static void main(String[] args) {
		Point point1 = new Point("Point 1", 10, 10);
		Point point2 = new Point("Point 2", 8, 7);
		Point point3 = new Point("Point 3", 12, 6);

		Point[] points = { point1, point2, point3 };
		System.out.println("--------------");
		System.out.println("Current data");
		System.out.println("--------------");
		printArrayPoints(points);

		Point[] sortPoints = sortPoints(points);
		System.out.println("----------");
		System.out.println("Sort data");
		System.out.println("----------");
		printArrayPoints(sortPoints);
	}

	static Point[] sortPoints(Point[] coordinats) {
		for (int i = coordinats.length; i > 1; i--) {
			for (int j = 1; j < i; j++) {
				int z = (int) Math.pow(coordinats[j - 1].x, 2) + (int) Math.pow(coordinats[j - 1].y, 2);
				int z1 = (int) Math.pow(coordinats[j].x, 2) + (int) Math.pow(coordinats[j].y, 2);
				if (z > z1) {
					Point temp = coordinats[j - 1];
					coordinats[j - 1] = coordinats[j];
					coordinats[j] = temp;
				}
			}
		}
		return coordinats;
	}

	static void printArrayPoints(Point[] points) {
		for (Point point : points) {
			System.out.println("Name: " + point.name + "\nX: " + point.x + "\nY: " + point.y + "\nPoint: "
					+ ((point.x * point.x) + (point.y * point.y)));
			System.out.println();
		}
	}
}
