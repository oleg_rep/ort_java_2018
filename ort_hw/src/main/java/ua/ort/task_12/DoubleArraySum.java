package ua.ort.task_12;

import java.util.Random;

public class DoubleArraySum {

	public static void main(String[] args) {
		int[][] array = new int[10][10];
		Random rand = new Random();
		int sum = 0;

		for (int i = 0; i < array.length; i++) {
			for (int j = 0; j < array.length; j++) {
				array[i][j] = rand.nextInt(10);
			}
		}

		for (int i = 0; i < array.length; i++) {
			for (int j = 0; j < array.length; j++) {
				sum += array[i][j];
			}
		}
		System.out.println("Сумма: " + sum);

	}
}
