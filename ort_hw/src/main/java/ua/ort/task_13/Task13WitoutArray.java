package ua.ort.task_13;

import java.util.Arrays;

public class Task13WitoutArray {

// if variable isMax is true - print max value else - print min
	static int countPupil(int[][] pupil, boolean isMax) {
		int[] pupilCounter = new int[4];
		int tmpIndex = pupilCounter[0];
		int minMax = 0;

		if (!isMax) {
			minMax = 99999;
		} else {
			minMax = tmpIndex;
		}

		for (int i = 0; i < pupil.length; i++) {
			for (int j = 0; j < pupil[0].length; j++) {
				tmpIndex += pupil[i][j];
			}
			pupilCounter[i] = tmpIndex;

			if (!isMax) {
				if (tmpIndex < minMax) {
					minMax = tmpIndex;
				}
			} else {
				if (tmpIndex > minMax) {
					minMax = tmpIndex;
				}
			}
			tmpIndex = 0;
		}
		return minMax;
	}

	public static void main(String[] args) {
		int[][] pupilArray = Task13WithArray.fillPupil();
		Task13WithArray.printPuple(pupilArray);

		System.out.println();
		System.out.println("\nСамая большая группа: " + countPupil(pupilArray, true) + " человек");
		System.out.println("Самая маленькая группа: " + countPupil(pupilArray, false) + " человек");
	}
}
