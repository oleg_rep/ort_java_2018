package ua.ort.task_13;

import java.util.Arrays;
import java.util.Random;

public class Task13WithArray {

	static int[][] fillPupil() {
		Random rand = new Random();
		int[][] pupilArray = new int[4][11];
		for (int i = 0; i < pupilArray.length; i++) {
			for (int j = 0; j < pupilArray[0].length; j++) {
				pupilArray[i][j] = rand.nextInt(20);
			}
		}
		return pupilArray;
	}

	static int[] countPupil(int[][] pupil) {
		int tmpIndex = 0;
		int[] pupilCounter = new int[4];

		for (int i = 0; i < pupil.length; i++) {
			for (int j = 0; j < pupil[0].length; j++) {
				tmpIndex += pupil[i][j];
			}
			pupilCounter[i] = tmpIndex;
			tmpIndex = 0;
		}
		return pupilCounter;
	}

	static int minMax(int[] pupilNumber, boolean isMax) {
		int count = pupilNumber[0];

		for (int i = 0; i < pupilNumber.length; i++) {
			if (!isMax) {
				if (pupilNumber[i] < count) {
					count = pupilNumber[i];
				}
			} else {
				if (pupilNumber[i] > count) {
					count = pupilNumber[i];
				}
			}
		}
		return count;
	}

	static void printPuple(int[][] puple) {
		for (int i = 0; i < puple.length; i++) {
			for (int j = 0; j < puple[0].length; j++) {
				System.out.print(puple[i][j] + " ");
			}
			System.out.println();
		}
	}

	public static void main(String[] args) {
		int[][] array = fillPupil();
		int[] pupilNumber = countPupil(array);

		printPuple(array);
		System.out.println();
		System.out.println("Самая большая группа: " + minMax(pupilNumber, true) + " человек");
		System.out.println("Самая маленькая группа: " + minMax(pupilNumber, false) + " человек");
	}
}
