package ua.ort.Task_6;

public class TraficLight {

	public static void main(String[] args) {
		int t = 70;

		if (t < 60) {
			if (t % 5 <= 2 && t % 5 > 0) {
				System.out.println("Зелёный");
			} else {
				System.out.println("Красный");
			}
		} else {
			System.err.println("Введите число меньше 60");
		}
	}
}
