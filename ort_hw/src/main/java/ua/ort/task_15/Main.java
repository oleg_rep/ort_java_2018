package ua.ort.task_15;

class Point {
	
	String name = null;
	int x = 0;
	int y = 0;
}

public class Main {

	public static void main(String[] args) {
		Point point1 = new Point();
		point1.name = "Point 1";
		point1.x = 10;
		point1.y = 10;

		Point point2 = new Point();
		point2.name = "Point 2";
		point2.x = 8;
		point2.y = 7;

		Point point3 = new Point();
		point3.name = "Point 3";
		point3.x = 12;
		point3.y = 6;

		Point[] points = { point1, point2, point3 };
		System.out.println("--------------");
		System.out.println("Current data");
		System.out.println("--------------");
		printArrayPoints(points);
		
		Point[] sortPoints = sortPoints(points);
		System.out.println("----------");
		System.out.println("Sort data");
		System.out.println("----------");
		printArrayPoints(sortPoints);
	}

	static Point[] sortPoints(Point[] coordinats) {
		for (int i = coordinats.length; i > 1; i--) {
			for (int j = 1; j < i; j++) {
				int z = (int) Math.pow(coordinats[j - 1].x, 2) + (int) Math.pow(coordinats[j - 1].y, 2);
				int z1 = (int) Math.pow(coordinats[j].x, 2) + (int) Math.pow(coordinats[j].y, 2);
				if (z > z1) {
					Point temp = coordinats[j - 1];
					coordinats[j - 1] = coordinats[j];
					coordinats[j] = temp;
				}
			}
		}
		return coordinats;
	}

	static void printArrayPoints(Point[] points) {
		for (Point point : points) {
			System.out.println("Name: " + point.name + "\nX: " + point.x + "\nY: " + point.y + "\nPoint: "
					+ ((point.x * point.x) + (point.y * point.y)));
			System.out.println();
		}
	}
}
