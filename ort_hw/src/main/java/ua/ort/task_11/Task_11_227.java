package ua.ort.task_11;

public class Task_11_227 {
	public static void main(String[] args) {
		int[] dataWearher = { 2, 3, 1, -4, 5, 6 - 2 };
		int rain = 0;
		int snow = 0;

		for (int i = 0; i < dataWearher.length; i++) {
			if (dataWearher[i] < 0) {
				snow++;
			} else {
				rain++;
			}
		}
		System.out.println("snow: " + snow);
		System.out.println("rain: " + rain);
	}
}
