package ua.ort.task_11;

public class Task_11_204 {

	public static void main(String[] args) {
		// с парой
		int[] digitsArray = { 2, 4, 5, 9, 2, 1, 3 };

//		без пары
//		int[] digitsArray = { 2, 4 }; 

		String result = null;

		for (int i = 0; i < digitsArray.length - 1; i++) {
			if ((digitsArray[i] % 2 != 0) && (digitsArray[i + 1] % 2 != 0)) {
				result = i + " and " + (i + 1);
				break;
			} else {
				result = "пар не найдено";
			}
		}
		System.out.println(result);
	}
}
