package ua.ort.task_11;

import java.util.Arrays;

public class Task_11_245 {

	public static void main(String[] args) {
		int[] baseArray = { 1, -6, -7, 5, 6, -45, 9 };
		int[] changedArray = new int[baseArray.length];
		int j = 0;
		int k = baseArray.length - 1;

		for (int i = 0; i < changedArray.length; i++) {
			if (baseArray[i] < 0) {
				changedArray[j] = baseArray[i];
				j++;
			} else {
				changedArray[k] = baseArray[i];
				k--;
			}
		}

		System.out.println(Arrays.toString(changedArray));
	}
}
