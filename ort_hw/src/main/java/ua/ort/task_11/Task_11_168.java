/*
 * 11.168.*Вставить число n между всеми соседними элементами, имеющими одинаковый знак.
 * 
 * */

package ua.ort.task_11;

import java.util.Arrays;
import java.util.Random;

public class Task_11_168 {

	public static void main(String[] args) {
		int[] arr = { -3, -1, -2, 3, 7, -4, -5, -7 };
		int sizeCounter = 0;
		int j = 0;
		int insertedDigit = 333;

		for (int i = 0; i < arr.length - 1; i++) {
			if ((arr[i] < 0 && arr[i + 1] < 0) || (arr[i] > 0 && arr[i + 1] > 0)) {
				sizeCounter++;
			}
		}
		
		int resultArraySize = sizeCounter + arr.length;
		int[] resultArray = new int[resultArraySize];

		for (int i = 0; i < arr.length - 1; i++) {
			resultArray[j] = arr[i];
			j++;
			if ((arr[i] < 0 && arr[i + 1] < 0) || (arr[i] > 0 && arr[i + 1] > 0)) {
				resultArray[j] = insertedDigit;
				j++;
				resultArray[j] = arr[i];
			}
		}
		System.out.println();
		System.out.println("Original array: " + Arrays.toString(arr));
		System.out.println("Changed array: " + Arrays.toString(resultArray));
	}
}
